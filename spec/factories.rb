FactoryGirl.define do
  factory :user do
    name      "awochna"
    email     "awochna@awochna.com"
    password  "foobar"
    password_confirmation "foobar"
  end
  factory :character do
    user
    name         "Kimblee"
    nickname     "Kimblee"
    level        12
    character_class :alchemist
    subclass     :mind_chemist
    race         :human
    alignment    "Chaotic Neutral"
    deity        "Science"
    size         :medium
    age          23
    gender       "Male"
    height       "5'9\""
    weight       "145"
    eyes         "Blue"
    hair         "Blue"
    strength     12
    dexterity    19
    constitution 12
    intelligence 28
    wisdom       10
    charisma     8
    racial_traits ([ :bonus_feat, :skilled ])
    favored_class :alchemist
  end
  factory :basic_character, class: Character do
    user
    name            "Kaia"
    race            :human
    character_class :alchemist
    size            :medium
  end
end
