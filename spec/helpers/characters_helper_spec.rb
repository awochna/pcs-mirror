require 'spec_helper'

describe CharactersHelper do
  
  describe "mod_calc" do
    
    it "should round down" do
      expect(helper.mod_calc(29)).to eq 9
    end

    it "should give same answer when point increased to odd" do
      expect(helper.mod_calc(29)).to eq helper.mod_calc(28)
    end

    it "should give 0 when score is 10" do
      expect(helper.mod_calc(10)).to eq 0
    end

    it "should give negative modifier when score is less than 10" do
      expect(helper.mod_calc(8)).to eq -1
    end
  end

  describe "defining saves" do

    it "alchemist should return fort and ref" do
      expect(helper.define_major_saves(:alchemist)).to eq [:fortitude, :reflex]
    end

    describe "assign right major save" do
      before do
        @fort = %i(barbarian cavalier fighter samurai)
        @fort_ref = %i(alchemist gunslinger ranger)
        @fort_will = %i(antipaladin cleric druid inquisitor magus paladin)
        @ref = %i(ninja rogue)
        @ref_will = %i(bard monk)
        @will = %i(oracle sorcerer summoner witch wizard)
      end

      it "for fighter classes" do
        @fort.each do |character_class|
          expect(helper.define_major_saves(character_class)).to eq [:fortitude]
        end
      end

      it "for light fighter classes" do
        @fort_ref.each do |character_class|
          expect(helper.define_major_saves(character_class)).to eq [:fortitude, :reflex]
        end
      end

      it "for religious fighters" do
        @fort_will.each do |character_class|
          expect(helper.define_major_saves(character_class)).to eq [:fortitude, :will]
        end
      end

      it "for skill monkeys" do
        @ref.each do |character_class|
          expect(helper.define_major_saves(character_class)).to eq [:reflex]
        end
      end

      it "for religious light fighters" do
        @ref_will.each do |character_class|
          expect(helper.define_major_saves(character_class)).to eq [:reflex, :will]
        end
      end

      it "for casters" do
        @will.each do |character_class|
          expect(helper.define_major_saves(character_class)).to eq [:will]
        end
      end
    end
  end

  describe "assigning base saves" do
    before do
      @fort = %i(barbarian cavalier fighter samurai)
      @fort_result = { fortitude: 8, reflex: 4, will: 4 }
      @fort_ref = %i(alchemist gunslinger ranger)
      @fort_ref_result = { fortitude: 8, reflex: 8, will: 4 }
      @fort_will = %i(antipaladin cleric druid inquisitor magus paladin)
      @fort_will_result = { fortitude: 8, reflex: 4, will: 8 }
      @ref = %i(ninja rogue)
      @ref_result = { fortitude: 4, reflex: 8, will: 4 }
      @ref_will = %i(bard monk)
      @ref_will_result = { fortitude: 4, reflex: 8, will: 8 }
      @will = %i(oracle sorcerer summoner witch wizard)
      @will_result = { fortitude: 4, reflex: 4, will: 8 }
    end

    it "for fighters" do
      @fort.each do |character_class|
        expect(helper.base_saves_calc(character_class, 12)).to eq @fort_result
      end
    end

    it "for light fighters" do
      @fort_ref.each do |character_class|
        expect(helper.base_saves_calc(character_class, 12)).to eq @fort_ref_result
      end
    end

    it "for religious fighters" do
      @fort_will.each do |character_class|
        expect(helper.base_saves_calc(character_class, 12)).to eq @fort_will_result
      end
    end

    it "for skill monkeys" do
      @ref.each do |character_class|
        expect(helper.base_saves_calc(character_class, 12)).to eq @ref_result
      end
    end

    it "for light religious fighters" do
      @ref_will.each do |character_class|
        expect(helper.base_saves_calc(character_class, 12)).to eq @ref_will_result
      end
    end

    it "for casters" do
      @will.each do |character_class|
        expect(helper.base_saves_calc(character_class, 12)).to eq @will_result
      end
    end
  end

  describe "calculating full saves" do
    let(:character) { FactoryGirl.create(:character) }

    it "should properly caclulate Kimblee's saves" do
      result = { fortitude: 9, reflex: 12, will: 4 }
      expect(helper.saves_calc(character.character_class, character.level, character.constitution, character.dexterity, character.wisdom)).to eq result
    end
  end

  describe "base attack bonus" do
    let(:character) { FactoryGirl.create(:character) }

    it "should properly calculate base attack bonus for Kimblee" do
      expect(helper.bab_calc(character.character_class, character.level)).to eq 9
    end

    it "should give correct response for a level 15 full-bab" do
      expect(helper.bab_calc(:fighter, 15)).to eq 15
    end

    it "should give correct response for a level 15 half-bab" do
      expect(helper.bab_calc(:wizard, 15)).to eq 7
    end

    it "should round down to give correct edge responses" do
      expect(helper.bab_calc(:alchemist, 12)).to eq helper.bab_calc(:alchemist, 13)
      expect(helper.bab_calc(:wizard, 11)).to_not eq helper.bab_calc(:wizard, 12)
    end
  end

  describe "calculating full attack bonus" do

    it "should give bab for melee with no ability score bonus" do
      expect(helper.attack_bonus_calc(:fighter, 12, 10)).to eq 12
    end

    it "should give modified bonus for melee with ability score bonus" do
      expect(helper.attack_bonus_calc(:fighter, 12, 22)).to eq 18
    end
  end
end
