require 'spec_helper'

include CharacterSize

describe CharacterSize do

  describe "determining" do
    before do
      @medium = %i(dwarf elf half-elf half-orc human aasimar catfolk dhampir drow
                   fetchling hobgoblin ifrit orc oread sylph tengu tiefling undine
                   changeling duergar gillman grippli kitsune merfolk nagaji
                   samsaran strix)
      @small = %i(gnome halfling goblin kobold ratfolk)
    end

    it "for medium races" do
      @medium.each do |race|
        expect(correct_size(race)).to eq :medium
      end
    end

    it "for small races" do
      @small.each do |race|
        expect(correct_size(race)).to eq :small
      end
    end
  end
end