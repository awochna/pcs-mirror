require 'spec_helper'

describe User do

  before do
    @example_user = User.new(name: "Example User", email: "user@example.com",
                     password: "foobar", password_confirmation: "foobar")
  end 
  let(:user2) { FactoryGirl.create(:user) }

  subject { @example_user }

  it { should respond_to(:name) }
  it { should respond_to(:email) }
  it { should respond_to(:password_digest) }
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }
  it { should respond_to(:remember_token) }
  it { should respond_to(:authenticate) }

  it { should be_valid }

  describe "when name is blank" do
    before { @example_user.name = "" }

    it { should_not be_valid }
  end

  describe "when email is missing" do
    before { @example_user.email = "" }

    it { should_not be_valid }
  end

  describe "when name is too long" do
    before { @example_user.name = "a" * 51 }

    it { should_not be_valid }
  end

  describe "when name is too short" do
    before { @example_user.name = "a" * 4 }

    it { should_not be_valid }
  end

  describe "when email is taken" do
    before do
      user_with_same_email = @example_user.dup
      user_with_same_email.email = "some-other@example.com"
      user_with_same_email.save
    end

    it { should_not be_valid }
  end

  describe "when email format is invalid" do
    it "should be invalid" do
      addresses = %w[user@foo,com user_at_foo.org example.user@foo. foo@bar_baz.com foo@bar+baz.com]
      addresses.each do |invalid_address|
        @example_user.email = invalid_address
        expect(@example_user).to_not be_valid 
      end
    end
  end

  describe "when email format is valid" do
    it "should be valid" do
      addresses = %w[user@foo.COM A_US-ER@f.b.org frst.lst@foo.jp a+b@baz.cn]
      addresses.each do |valid_address|
        @example_user.email = valid_address
        expect(@example_user).to be_valid
      end
    end
  end

  describe "when name address is taken" do
    before do
      user_with_same_name = @example_user.dup
      user_with_same_name.name = "Other Guy"
      user_with_same_name.save
    end

    it { should_not be_valid }
  end

  describe "when password" do

    describe "is not present" do
      before do
        @example_user = User.new(name: "Example", email: "user@example.com",
                         password: " ", password_confirmation: " ")
      end

      it { should_not be_valid }
    end

    describe "doesn't match confirmation" do
      before { @example_user.password_confirmation = "mismatch" }
      it { should_not be_valid }
    end
    
    describe "is too short" do
      before { @example_user.password = @example_user.password_confirmation = "a" * 5 }
      it { should be_invalid }
    end
  end

  describe "return value of authenticate method" do
    before { @example_user.save }
    let(:found_user) { User.find_by(name: @example_user.name) }

    describe "with valid password" do
      it { should eq found_user.authenticate(@example_user.password) }
    end

    describe "with invalid password" do
      let(:user_for_invalid_password) { found_user.authenticate("invalid") }

      it { should_not eq user_for_invalid_password }
      specify { expect(user_for_invalid_password).to be_false }
    end
  end

  describe "remember token" do
    before { @example_user.save }
    its(:remember_token) { should_not be_blank }
  end

  describe "owns characters" do
    
    describe "no characters" do
      it "should return an empty character set" do
        expect(user2.characters).to be_empty
      end
    end

    describe "a character" do
      before do
        FactoryGirl.create(:character, user_id: user2.id)
      end
      it "should return a non-empty character set" do
        expect(user2.characters).to_not be_empty
      end
    end
  end
end
