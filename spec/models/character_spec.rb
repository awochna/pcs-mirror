require 'spec_helper'

describe Character do
  
  let(:character) { FactoryGirl.create(:character) }

  subject { character }

  it { should respond_to(:user) }
  it { should respond_to(:user_id) }
  it { should respond_to(:name) }
  it { should respond_to(:nickname) }
  it { should respond_to(:experience) }
  it { should respond_to(:level) }
  it { should respond_to(:character_class) }
  it { should respond_to(:subclass) }
  it { should respond_to(:race) }
  it { should respond_to(:alignment) }
  it { should respond_to(:deity) }
  it { should respond_to(:size) }
  it { should respond_to(:age) }
  it { should respond_to(:gender) }
  it { should respond_to(:height) }
  it { should respond_to(:weight) }
  it { should respond_to(:eyes) }
  it { should respond_to(:hair) }
  it { should respond_to(:strength) }
  it { should respond_to(:dexterity) }
  it { should respond_to(:constitution) }
  it { should respond_to(:intelligence) }
  it { should respond_to(:wisdom) }
  it { should respond_to(:charisma) }
  it { should respond_to(:racial_traits) }
  it { should respond_to(:favored_class) }
  
  it { should be_valid }

  describe "without a name" do
    before { subject.name = " " }
    it { should_not be_valid }
  end

  describe "with a negative level" do
    before { subject.level = -1 }
    it { should_not be_valid }
  end

  describe "with an empty class" do
    before { subject.character_class = " " }
    it { should_not be_valid }
  end

  describe "with an improper size category" do
    before { subject.size = :super_huge }
    it "should get corrected" do
      expect(subject.size = :medium)
    end
  end

  # Perhaps from Enlarge Person
  describe "with incorrect size category" do
    before { subject.size = :large }
    it { should be_valid }
    it "should not change" do
      subject.save
      expect(subject.size).to eq :large
    end
  end

  describe "when ability score for" do
    ability_scores = %w(strength dexterity constitution
                        intelligence wisdom charisma)
    ability_scores.each do |ability_score|

      specify "#{ability_score} is 0" do
        subject.send(:"#{ability_score}=", 0)
        expect(subject).to be_invalid
      end
    end
  end

  describe "creation" do

    let(:basic_character) { FactoryGirl.create(:basic_character) }

    subject { basic_character }

    describe "should fall back on intelligent defaults" do

      its(:level) { should eq 1 }
      ability_scores = [:strength, :dexterity, :constitution,
                        :intelligence, :wisdom, :charisma]
      ability_scores.each do |ability_score|
        its(:"#{ability_score}") { should eq 10 }
      end
    end
  end
end 
