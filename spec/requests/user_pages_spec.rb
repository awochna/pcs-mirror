require 'spec_helper'

describe "User Pages" do

  subject { page }

  describe "profile page" do
    let(:character) { FactoryGirl.build(:character) }
    let(:user) { User.find_by(id: character.user_id) }
    before { visit user_path(user) }

    it { should have_content(user.name) }
    it { should have_title(user.name) }
    it { should have_link("Create a new character") }
    it { should_not have_content("Kimblee") }

    describe "after character creation" do
      before do
        character.save!
        visit user_path(user)
      end
      
      it { should have_content(user.name) }
      it { should have_content 'Kimblee' }
      it { should_not have_content "You don't have any characters" }
    end
  end

  describe "signup" do
    
    before { visit signup_path }

    let(:submit) { "Create my account" }

    describe "with invalid information" do
      it "should not create a user" do
        expect { click_button submit }.not_to change(User, :count)
      end
    end

    describe "with valid information" do
      before do
        fill_in "Name",         with: "Example User"
        fill_in "Email",        with: "user@example.com"
        fill_in "Password",     with: "foobar"
        fill_in "Confirmation", with: "foobar"
      end

      it "should create a user" do
        expect { click_button submit }.to change(User, :count).by(1)
      end

      describe "after saving the user" do
        before { click_button submit }
        let(:user) { User.find_by(email: 'user@example.com') }

        it { should have_link('Sign out') }
        it { should have_title(user.name) }
        it { should have_selector('div.alert.alert-success', text: 'Welcome') }
      end
    end
  end
end
