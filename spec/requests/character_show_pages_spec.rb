require 'spec_helper'

describe "Character Show Pages" do

  subject { page }

  let(:character) { FactoryGirl.create(:character) }

  before { visit user_character_path(character.user_id, character.slug) }

  it { should have_content('Kimblee') }
  it { should have_selector(".int-mod", "9") }

end
