require 'spec_helper'

describe StaticPagesController do

  subject { page }

  describe "Home page" do
    before { visit root_path }

    it { should have_selector('h1', text: 'Pathfinder Character Sheet App') }
    it { should have_title(full_title('')) }
    it { should_not have_title(full_title('| Home')) }
    it { should have_content('Sign up') }

    it "should have the content 'Pathfinder Character Sheet App'" do
      visit root_path
      expect(page).to have_content('Pathfinder Character Sheet App')
    end
  end

  describe "Help page" do
    before { visit help_path }
    
    it { should have_content('Need some help?') }
    it { should have_title(full_title('Help')) }
    it { should have_content('Paizo') } # TODO: Turn into a link to paizo's site
  end

  describe "About page" do
    before { visit about_path }

    it { should have_content('About') }
    it { should have_title(full_title('About')) }
  end
end
