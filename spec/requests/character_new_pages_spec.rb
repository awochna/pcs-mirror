require 'spec_helper'

describe "New Character Pages" do
  let(:user) { FactoryGirl.create(:user) }

  before { visit user_path(user) }

  subject { page }

  it { should have_link "Create a new character" }

  describe "making a new character" do
    before { click_link "Create a new character" }

    describe "has the right header" do
      it { should have_selector('h1', 'Create a new character') }
    end

    describe "accepts input" do
      before do
        fill_in 'Name',  with: 'Kaia'
        fill_in 'Race',  with: 'Human'
        fill_in 'Class', with: 'Alchemist'
      end

      it "should create a character" do
        expect { click_button "Create character" }.to change(Character, :count).by(1)
      end

      describe "should redirect to character show" do
        before { click_button "Create character" }

        it "should redirect to character show" do
          expect(subject).to have_title('Kaia')
        end
      end
    end
  end
end
