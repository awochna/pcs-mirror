Feature: user

  Scenario: invalid sign up
    Given I am at the signup page
    When I click submit
    Then I should see errors

  Scenario: valid sign up
    Given I am at the signup page
    When I fill in valid information
    And I click submit
    Then I should see success