Given /^I am at the signup page$/ do
  visit signup_path
end

When /^I click submit$/ do
  click_button "Create my account"
end

Then /^I should see errors$/ do
  expect(page).to have_selector(".alert-error")
end

When /^I fill in valid information$/ do
  fill_in "Name",         with: "example"
  fill_in "Email",        with: "user@example.com"
  fill_in "Password",     with: "foobar"
  fill_in "Confirmation", with: "foobar"
end

Then /^I should see success$/ do
  expect(page).to have_selector(".alert-success")
end