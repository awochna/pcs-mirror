README
======

Online application for helping people keep track of, and eventually build, their character sheets for the Pathfinder Role Playing Game.

Stories
-------

Iteration 1 (0.1):

1. Visitor can learn more without signing up
2. Visitor can sign up as a user
3. User can see their characters from their profile page

Iteration 2 (0.2):

1. Users can see their character like a character sheet
2. Users can create characters
3. Users can edit their characters

Iteration 3 (1.0):

1. Users can level up their characters through a guided leveling system.