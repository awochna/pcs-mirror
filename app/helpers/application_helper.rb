module ApplicationHelper

  # Returns the full title on a per-page basis.
  def full_title(page_title)
    base_title = "Pathfinder Character Sheet App"
    if page_title.empty?
      base_title
    else
      "#{base_title} | #{page_title}"
    end
  end

  def prettify_classes(classes)
    output = "<ul> /n"
    classes.each do |key, value|
      output += "<li>#{key.to_s.humanize.titleize}: #{value}</li>"
    end
    output += "</ul>"
    return output
  end
end
