module CharactersHelper
  
  def mod_calc(score)
    (score - 10) / 2
  end

  def define_major_saves(character_class)
    fort = %i(barbarian cavalier fighter samurai)
    fort_ref = %i(alchemist gunslinger ranger)
    fort_will = %i(antipaladin cleric druid inquisitor magus paladin)
    ref = %i(ninja rogue)
    ref_will = %i(bard monk)
    will = %i(oracle sorcerer summoner witch wizard)
    if character_class.in? fort
      @major_save = [:fortitude]
    elsif character_class.in? fort_ref
      @major_save = [:fortitude, :reflex]
    elsif character_class.in? fort_will
      @major_save = [:fortitude, :will]
    elsif character_class.in? ref
      @major_save = [:reflex]
    elsif character_class.in? ref_will
      @major_save = [:reflex, :will]
    elsif character_class.in? will
      @major_save = [:will]
    end
  end

  def base_saves_calc(character_class, level)
    base_saves = { fortitude: 0, reflex: 0, will: 0 }
    major_saves = define_major_saves(character_class)
    base_saves.each do |key, value|
      if key.in? major_saves
        base_saves[key] = level * 2 / 3
      else
        base_saves[key] = level / 3
      end
    end
  end

  def saves_calc(character_class, level, con, dex, wis)
    base_saves = { fortitude: 0, reflex: 0, will: 0 }
    base_saves = base_saves_calc(character_class, level)
    save_mod = { fortitude: con, reflex: dex, will: wis }
    save_mod.each do |key, value|
      base_saves[:"#{key}"] += mod_calc(value)
    end
    return base_saves
  end

  def bab_calc(character_class, level)
    full_bab = [:barbarian, :fighter, :paladin, :ranger, :cavalier, :gunslinger, :antipaladin, :samurai]
    three_quarter_bab = [:bard, :cleric, :druid, :monk, :rogue, :alchemist, :inquisitor, :magus, :oracle, :summoner, :ninja]
    half_bab = [:sorcerer, :wizard, :witch]
    if character_class.in? full_bab
      level
    elsif character_class.in? three_quarter_bab
      level * 3 / 4
    elsif character_class.in? half_bab
      level / 2
    end
  end

  def attack_bonus_calc(character_class, level, ability_score)
    bab_calc(character_class, level) + mod_calc(ability_score)
  end

end
