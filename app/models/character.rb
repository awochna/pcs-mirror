class Character < ActiveRecord::Base

  # Associations
  belongs_to :user

  # Helpers
  SIZE_MSG = "must be Tiny, Small, Medium, Large, Huge, Gargantuan, or Colossal"
  SIZE_CATEGORIES = %i(tiny small medium large huge gargantuan colossal)
  non_zero_number_fields = %i(level strength dexterity constitution 
                              intelligence wisdom charisma)

  # Before validation
  before_validation :generate_slug
  before_validation :generate_ability_scores
  before_validation :set_first_level

  # Validations
  validates :size, inclusion: { in: SIZE_CATEGORIES, message: SIZE_MSG }
  validates :name, :character_class, presence: true
  validates :slug, uniqueness: true, presence: true
  non_zero_number_fields.each do |attribute|
    validates attribute, presence: true, numericality: { greater_than: 0 }
  end

  # Serialization
  serialize :character_class
  serialize :subclass
  serialize :racial_traits
  serialize :favored_class

  # Methods
  # def character_class=(input)
  #   input.to_sym
  # end

  def generate_ability_scores
    self.strength     ||= 10
    self.dexterity    ||= 10
    self.constitution ||= 10
    self.intelligence ||= 10
    self.wisdom       ||= 10
    self.charisma     ||= 10
  end

  def set_first_level
    self.level ||= 1
  end

  def to_param
    slug
  end

  def generate_slug
    self.slug ||= name.parameterize
  end

end
