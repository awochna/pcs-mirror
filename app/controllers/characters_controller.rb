class CharactersController < ApplicationController

  include CharacterSize

  # before_filter :find_character, only: [:show, :edit, :update, :destroy]

  def new
    @character = Character.new()
  end

  def create
    params[:character][:race].downcase.to_sym
    params[:character][:size] ||= correct_size(params[:character][:race])
    @character = Character.new(character_params)
    @user = User.find_by_slug(params[:user_id])
    @character.user_id = @user.id
    if @character.save!
      flash[:success] = "#{@character.name.capitalize} has been created!"
      redirect_to user_character_url(@character.user.slug, @character) 
    else
      render 'new'
    end
  end

  def show
    @character = Character.find_by_slug!(params[:id])
    @user = User.find_by(id: @character.user_id)
  end

  def edit
  end

  private

  def character_params
    params.require(:character).permit(:name, :nickname, :race, :character_class,
                                      :subclass, :deity, :age, :gender, :height,
                                      :weight, :eyes, :hair, :strength, 
                                      :dexterity, :constitution, :intelligence, 
                                      :wisdom, :charisma, :size)
  end

  def character
    @character ||= Character.find_by_slug!(params[:id])
  end
  helper_method :character

end
