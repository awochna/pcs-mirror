class UsersController < ApplicationController

  def new
    @user = User.new
  end
  
  def show
    @user = User.find_by_slug!(params[:id])
  end

  def create
    @user = User.new(user_params)
    if @user.save
      sign_in @user
      flash[:success] = "Welcome to the Pathfinder Character Sheet App!"
      redirect_to @user
    else
      render 'new'
    end
  end

  private

    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end

    def user
      @user ||= User.find_by_slug!(params[:id])
    end
    helper_method :user
end
