module CharacterSize

  def correct_size(race)
    small = %i(gnome halfling goblin kobold ratfolk)
    small.index(race) ? :small : :medium
  end

end