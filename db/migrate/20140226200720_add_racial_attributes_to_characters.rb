class AddRacialAttributesToCharacters < ActiveRecord::Migration
  def change
    add_column :characters, :racial_traits, :text
    add_column :characters, :favored_class, :text
  end
end
