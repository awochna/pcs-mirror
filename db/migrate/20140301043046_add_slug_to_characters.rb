class AddSlugToCharacters < ActiveRecord::Migration
  def change
    add_column :characters, :slug, :text
    add_index :characters, :slug
  end
end
