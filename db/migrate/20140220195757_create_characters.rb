class CreateCharacters < ActiveRecord::Migration
  def change
    create_table :characters do |t|
      t.text :name
      t.text :nickname
      t.integer :level
      t.text :character_class
      t.text :subclass
      t.text :race
      t.text :alignment
      t.text :deity
      t.text :size
      t.text :age
      t.text :gender
      t.text :height
      t.text :weight
      t.text :eyes
      t.text :hair

      t.timestamps
    end
  end
end
